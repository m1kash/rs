const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');
module.exports = {
  entry: './src/index.js',
  output: {
    filename: './app.bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      { enforce: 'pre', test: /\.js$/, loader: 'eslint-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /.(jpg|jpeg|png|svg)$/,
        use: ['file-loader'],
      },
    ],
  },
  plugins: [new HtmlWebpackPlugin({
    title: 'Piskel Animation Player',
    meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
  }),
  new JavaScriptObfuscator({
    rotateUnicodeArray: true,
  })],
};
