import html2canvas from 'html2canvas';
import App from './components/App';

const options = {
  currentTool: 'paint-bucket',
  currentColor: '#C4C4C4',
  prevColor: '#41F795',
};
const frames = [
  [{ bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] },
    { bgColor: '#E5E5E5', classes: ['figure', 'circle'] }],
  [{ bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#00ffff', classes: ['figure', 'circle'] },
    { bgColor: '#ffccee', classes: ['figure', 'circle'] },
    { bgColor: '#ff1200', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffccee', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffff00', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] }],
  [{ bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#00ffff', classes: ['figure', 'circle'] },
    { bgColor: '#ff1200', classes: ['figure', 'circle'] },
    { bgColor: '#00ffff', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffff00', classes: ['figure', 'circle'] },
    { bgColor: '#ffccee', classes: ['figure', 'circle'] },
    { bgColor: '#ffff00', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] }],
  [{ bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#00ffff', classes: ['figure', 'circle'] },
    { bgColor: '#ff1200', classes: ['figure', 'circle'] },
    { bgColor: '#00ffff', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ff1200', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffff00', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] }],
  [{ bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffccee', classes: ['figure', 'circle'] },
    { bgColor: '#00ffff', classes: ['figure', 'circle'] },
    { bgColor: '#ffccee', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffff00', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] },
    { bgColor: '#ffccee', classes: ['figure', 'circle'] },
    { bgColor: '#000', classes: ['figure', 'circle'] }],

];
const app = new App(options);
app.init(frames);
window.html2canvas = html2canvas;
