class AppView {
  constructor(state, figures) {
    this.state = state;
    this.figures = figures;
    this.init();
  }

  init() {
    this.renderFirst();
  }

  renderFirst() {
    const bodyElem = document.body;
    const options = this.state;

    bodyElem.innerHTML = `
    <div class="container">
      <header class="header white flex">
          <button class="nav-icon"></button>
          <h1 class="title">CodeJam - Pallete</h1>
          <div class="settings-icon"></div>
      </header>
      <main class="component padding-thirty margin-bottom-forty flex space-between">
          <aside class="sidebar">
              <div class="panel panel-tools white">
                  <button name="paint-bucket" class="button-default paint-bucket icon-padding">Paint bucket</button>
                  <button name="color-picker" class="button-default color-picker icon-padding">Choose color</button>
                  <button name="move" class="button-default move icon-padding">Move</button>
                  <button name="transform" class="button-default transform icon-padding">Transform</button>
              </div>
              <div class="panel panel-colors">
                  <button name="current_color" data-color="${options.currentColor}" class="button-default icon-padding color">
                      <span style="background: ${options.currentColor};"></span>
                      Current color
                  </button>
                  <button name="prev_color" data-color="${options.prevColor}" class="button-default icon-padding color">
                      <span style="background: ${options.prevColor};"></span>
                      Prev color
                  </button>
                  <hr>
                  <button data-color="#F74141" class="button-default icon-padding color">
                      <span style="background: #F74141;"></span>
                      red
                  </button>
                  <button data-color="#41B6F7" class="button-default icon-padding color">
                      <span style="background: #41B6F7;"></span>
                      blue
                  </button>
              </div>
          </aside>
          <aside class="frames panel">
          </aside>
          <section class="canvas">
          </section>
          <div class="preview">
            <a href="#" class="preview-full-screen">
              preview full screen
            </a>
            <input type="range" min="1" max="30" value="1" class="preview-slider" id="myRange">
          </div>
      </main>
    </div>
    `;
  }


  renderColors(currentColor) {
    const panelColors = document.querySelector('.panel-colors');
    const elementPrevColor = panelColors.querySelector('[name="prev_color"] span');
    const elementCurrentColor = panelColors.querySelector('[name="current_color"] span');
    const options = this.state;

    options.prevColor = options.currentColor;
    options.currentColor = currentColor;

    elementPrevColor.style.backgroundColor = options.prevColor;
    elementCurrentColor.style.backgroundColor = options.currentColor;
  }
}

export default AppView;
