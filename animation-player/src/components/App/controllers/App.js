import _ from 'lodash';
import AppView from '../views';
import Frame from '../../Frame/controllers';
import Preview from '../../Preview/controllers';
import '../../../../assets/sass/style.scss';

class App {
  constructor(state) {
    this.state = state;
  }

  init(dataFrames) {
    this.stateDrag = {};
    this.view = new AppView(this.state);
    this.frame = new Frame(dataFrames);
    this.preview = new Preview(this.frame.images);
    this.initHandles();
  }

  updateData() {
    const frameConroller = this.frame;
    const previewConroller = this.preview;
    const previewView = this.preview.view;
    const isDiff = _.difference(frameConroller.images, previewView.images);
    if (isDiff.length) {
      previewConroller.images = frameConroller.images;
      previewView.data = frameConroller.images;
    }
  }

  initHandles() {
    const tools = document.querySelector('.panel-tools');
    const panelColors = document.querySelector('.panel-colors');
    const paintArea = document.querySelector('.canvas');
    document.addEventListener('keydown', (event) => {
      if (event.key === 80) {
        this.state.currentTool = 'paint-bucket';
      } else if (event.key === 67) {
        this.state.currentTool = 'color-picker';
      } else if (event.key === 84) {
        this.state.currentTool = 'transform';
      } else if (event.key === 77) {
        this.state.currentTool = 'move';
        this.dragFirureEvent(true);
        return;
      }
      this.dragFirureEvent(false);
    });
    tools.addEventListener('click', (event) => {
      const currentElement = event.target;
      const elementName = currentElement.name;
      this.state.currentTool = elementName;
      if (elementName === 'move') {
        this.dragFirureEvent(true);
        return;
      }
      this.dragFirureEvent(false);
    });
    panelColors.addEventListener('click', (event) => {
      const currentElement = event.target;
      const elementData = currentElement.dataset;
      const elementColor = elementData.color;

      this.view.renderColors(elementColor);
    });
    paintArea.addEventListener('click', (event) => {
      const currentElement = event.target;
      const elementStyle = currentElement.style;
      const elementBackground = elementStyle.backgroundColor;

      if (currentElement.classList.contains('figure') !== true) {
        return;
      }

      if (this.state.currentTool === 'paint-bucket') {
        elementStyle.backgroundColor = this.state.currentColor;
      } else if (this.state.currentTool === 'color-picker') {
        this.view.renderColors(elementBackground);
      } else if (this.state.currentTool === 'transform') {
        currentElement.classList.toggle('circle');
      }
    });
    setInterval(this.updateData.bind(this), 5000);
  }

  dragFirureEvent(value) {
    const paintArea = document.querySelector('.canvas');
    const figures = paintArea.querySelectorAll('.figure');
    if (value) {
      [].forEach.call(figures, (figure) => {
        figure.setAttribute('draggable', 'true');

        figure.addEventListener('dragstart', this.dragStartElement.bind(this), false);
        figure.addEventListener('dragenter', this.dragEnterElement.bind(this), false);
        figure.addEventListener('dragend', this.dragEndElement.bind(this), false);
      });
    } else {
      [].forEach.call(figures, (figure) => {
        figure.setAttribute('draggable', 'false');

        figure.removeEventListener('dragstart', this.dragStartElement.bind(this), false);
        figure.removeEventListener('dragenter', this.dragEnterElement.bind(this), false);
        figure.removeEventListener('dragend', this.dragEndElement.bind(this), false);
      });
    }
  }

  dragStartElement(event) {
    const currentElement = event.target;

    this.stateDrag.moveElement = currentElement;
    this.stateDrag.movePrevElement = currentElement.nextSibling;
  }

  dragEnterElement(event) {
    const currentElement = event.target;

    this.stateDrag.appendElement = currentElement;
    this.stateDrag.appendPrevElement = currentElement.nextSibling;
  }

  dragEndElement() {
    const paintArea = document.querySelector('.canvas');
    paintArea.insertBefore(this.stateDrag.appendElement, this.stateDrag.movePrevElement);
    paintArea.insertBefore(this.stateDrag.moveElement, this.stateDrag.appendPrevElement);
  }
}

export default App;
