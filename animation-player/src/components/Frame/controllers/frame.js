import _ from 'lodash';
import FrameView from '../views';

class Frame {
  constructor(layers) {
    this.layers = layers;
    this.images = [];
    this.init();
    this.inithandles();
  }

  init() {
    this.view = new FrameView(this.layers);
    this.updateData(true);
  }

  updateData(isFirst) {
    const dataView = this.view.data;
    const dataImages = this.view.images;
    const isDiff = _.difference(dataImages, this.images);
    if (isDiff.length || isFirst) {
      this.layers = dataView;
      this.images = dataImages;
    }
  }

  inithandles() {
    const framesElem = document.querySelector('.frames');

    framesElem.addEventListener('click', this.handleFrame.bind(this));
    setInterval(this.updateData.bind(this), 5000);
  }

  handleFrame(e) {
    const viewComponent = this.view;
    const targetElem = e.target;
    const frame = targetElem.classList.contains('frame') ? targetElem : targetElem.parentElement;
    const dataIndex = +frame.dataset.index;
    e.preventDefault();
    if (targetElem.classList.contains('frame')) {
      viewComponent.changeActive(dataIndex, frame);
    }

    if (targetElem.classList.contains('frame-remove') && !targetElem.classList.contains('active')) {
      viewComponent.removeFrame(dataIndex, frame);
    }

    if (targetElem.classList.contains('frame-copy')) {
      viewComponent.copyFrame(dataIndex);
    }

    if (targetElem.classList.contains('frames-add')) {
      viewComponent.addFrame();
    }
    this.updateData();
  }
}

export default Frame;
