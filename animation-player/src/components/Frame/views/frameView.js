import html2canvas from 'html2canvas';

class FrameView {
  constructor(data) {
    this.data = data;
    this.images = [];
    this.defaultFrame = [...data[0]];
    this.renderFrames();
  }

  renderFrames() {
    const frames = document.querySelector('.frames');
    const dataFrame = this.data;
    const addButton = document.createElement('button');
    const background = {};
    frames.innerHTML = '';
    this.images = [];
    dataFrame.forEach((figures, index) => {
      if (index === 0) {
        FrameView.renderFigures(figures, '.canvas');
      }
      FrameView.renderFrame(index);
      background.index = index;
      this.createBackground(figures, index);
    });
    addButton.textContent = 'Add new frame';
    addButton.classList.add('frames-add');
    frames.appendChild(addButton);
  }

  addFrame() {
    const defaultData = this.defaultFrame;
    this.data.push(defaultData);
    this.renderFrames();
  }

  removeFrame(index, elem) {
    const startFrame = +index;
    this.data.splice(startFrame, 1);
    this.images.splice(startFrame, 1);
    elem.remove();
    this.renderFrames();
  }

  copyFrame(index) {
    const copyFrame = this.data[index];
    this.data.push(copyFrame);
    this.renderFrames();
  }

  changeActive(index, frame) {
    const frameActive = document.querySelector('.frame.active');
    const figures = this.data[index];
    frameActive.classList.remove('active');
    frame.classList.add('active');
    FrameView.renderFigures(figures, '.canvas');
  }

  static renderFigures(figures, elem) {
    const canvas = document.querySelector(elem);
    canvas.innerHTML = '';
    figures.forEach((item) => {
      const figure = document.createElement('div');
      const classesArr = item.classes;
      const background = item.bgColor;

      classesArr.forEach((val) => {
        figure.classList.add(val);
      });
      figure.style.backgroundColor = background;
      canvas.appendChild(figure);
    });
  }

  static renderFrame(index) {
    const frames = document.querySelector('.frames');
    const frame = document.createElement('div');
    const frameData = frame.dataset;
    frameData.index = index;
    frame.classList.add('frame');
    if (index === 0) {
      frame.classList.add('active');
    }
    frame.innerHTML = `
              <span class="frame-box frame-name">${index + 1}</span>
              <a href="#" class="frame-box frame-copy"></a>
              <a href="#" class="frame-box frame-remove"></a>
    `;
    frames.appendChild(frame);
  }

  createBackground(figures, index) {
    const bodyElem = document.body;
    const previewHide = document.createElement('div');
    const frameClass = `[data-index="${index}"]`;
    const frame = document.querySelector(frameClass);
    previewHide.classList.add(`demo-${index}`);
    previewHide.innerHTML = '<div class="canvas"></div>';
    previewHide.style.opacity = '0.0';
    bodyElem.appendChild(previewHide);
    FrameView.renderFigures(figures, `.demo-${index} .canvas`);
    const canvasDemoElem = document.querySelector(`.demo-${index} .canvas`);
    html2canvas(canvasDemoElem).then((canvas) => {
      const objBg = {};
      const demoElem = document.querySelector(`.demo-${index}`);
      const background = canvas.toDataURL('image/png');
      frame.style.backgroundImage = `url('${background}')`;
      demoElem.remove();
      objBg.index = index;
      objBg.background = background;
      this.images.push(objBg);
    });
  }
}

export default FrameView;
