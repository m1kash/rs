class PreviewView {
  constructor(data = []) {
    this.data = data;
    this.previousTime = 0;
    this.image = 0;
    this.frames = 1;
    this.initAnimation();
  }

  initAnimation() {
    this.loop(1000);
  }

  loop(duration) {
    const previewElem = document.querySelector('.preview');
    let startTime = performance.now();

    const step = () => {
      const frames = duration / this.frames;
      const lengthArr = this.data.length;
      const now = performance.now();
      const delta = now - startTime;

      requestAnimationFrame(step);

      if (lengthArr !== 0 && delta > frames) {
        startTime = now - (delta % frames);
        const dataImages = this.data;
        const image = dataImages[this.image];
        previewElem.style.backgroundImage = `url('${image.background}')`;
        if (lengthArr - 1 === this.image) {
          this.image = 0;
        } else {
          this.image += 1;
        }
      }
    };
    step();
  }

  static changeScreen(elem, button) {
    const buttonElem = button;

    if (!document.fullscreenElement) {
      buttonElem.textContent = 'exit full screen';
      elem.requestFullscreen().then(() => {
        buttonElem.textContent = 'exit full screen';
      });
    } else {
      document.exitFullscreen().then(() => {
        buttonElem.textContent = 'preview full screen';
      });
    }
  }
}

export default PreviewView;
