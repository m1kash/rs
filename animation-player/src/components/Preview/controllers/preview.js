import PreviewView from '../views';

class Preview {
  constructor(images) {
    this.images = images;
    this.view = new PreviewView(this.images);
    this.view.data = this.images;
    this.inithandles();
  }

  inithandles() {
    const rangeElem = document.querySelector('.preview-slider');
    const fullElem = document.querySelector('.preview-full-screen');

    rangeElem.addEventListener('click', this.handleRange.bind(this));
    fullElem.addEventListener('click', Preview.handleFull);
  }

  handleRange(e) {
    const valueFames = e.target.value;

    this.view.frames = +valueFames;
  }

  static handleFull(e) {
    const buttonElem = e.target;
    const previewElem = buttonElem.parentElement;

    PreviewView.changeScreen(previewElem, buttonElem);
  }
}

export default Preview;
